## App em Flutter/Odoo Clinical Engeenering Assistant

App para gerenciamento de Chamados e Atendimentos da Kimenz Equipamentos. App design is based on [Task Planner App](https://dribbble.com/shots/10951333/attachments/2566966?mode=media) designed by [Purrweb UI](https://dribbble.com/purrwebui).


Backend sendo conectado em Odoo

## Screenshots

  HomePage              |   Calendar Page | Novo Atendimento
:-------------------------:|:-------------------------:|:---------------------:
![](https://gitlab.com/popsolutions/kimenz-at/-/raw/master/screenshots/screenshot1.jpg?raw=true)|![](https://gitlab.com/popsolutions/kimenz-at/~/blob/master/screenshots/screenshot2.jpg?raw=true)|![](https://gitlab.com/popsolutions/kimenz-at//blob/master/screenshots/screenshot3.jpg?raw=true)


## Pull Requests

I welcome and encourage all pull requests. It usually will take me within 24-48 hours to respond to any issue or request.

## Created & Maintained By

[Sourav Kumar Suman](https://github.com/TheAlphaApp) 
[Marcos Mendez](https://www.linkedin.com/company/11553755/admin/notifications/all/)

