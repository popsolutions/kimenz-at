import 'package:flutter/material.dart';

class LightColors {
  static const Color kKimenzBlue = Color(0xFF065696);
  static const Color kLightYellow = Color(0xFF90C0E5);
  static const Color kLightYellow2 = Color(0xFF1E2E45);
  static const Color kPalePink = Color(0xFFFED4D6);

  static const Color kRed = Color(0xFF47A3A1);
  static const Color kLavender = Color(0xFFD5E4FE);
  static const Color kBlue = Color(0xFF6488E4);
  static const Color kLightGreen = Color(0xFFD9E6DC);
  static const Color kGreen = Color(0xFF309397);

  static const Color kDarkBlue = Color(0xFF0D253F);
  static const Color kDarkYellow = Color(0xFF075796);
  static const Color kLightWhite = Color(0xFFFFFFFF);
}
